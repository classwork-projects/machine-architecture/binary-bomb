# Binary Bomb #145

This project consisted of analyzing AMD64 (aka X64) instruction-set architecture. The assembly code was made up of a half-dozen or so C procedures deciphered through only an executable binary. Given the binary "bomb" made up of 6 phases, and a hidden secret phase that I discovered, the binary will be "defused" if each phase is provided the correct input on standard input or a defuse file passed at runtime. I successfully defused the bomb and described the process of analyzing each phase below.


## Phase Explanations:

To avoid further incidental explosions I set breakpoints on
the explode bomb statements, and the individual phases to safely step through the program and test inputs, with the TUI mode of GDB with layout split.  

**Phase 1** calls a function `strings_not_equal` that compares an input string to a stored string. If the strings do not match, the bomb explodes. For this bomb, the string it was looking for was "I am not part of the problem. I am a Republican", which was found in the strings of the executable by running the command, 'strings bomb'.    

**Phase 2** reads through input a total of 6 numbers, separated by spaces. A function named `read_six_numbers` reads them in, and checks to make sure the first number is 1. If the first number isn't 1, the bomb explodes, and if it is, it checks to make sure that the subsequent numbers are twice that of the previous input. The bomb will explode if these conditions aren't met.  
 

**Phase 3** involves two comparisons. It reads in two integers. The first comparison is to see that the first number is <=8. Once it passes this comparison, the second number is compared to a value based on the first input. If both integers do not pass the comparison, the bomb explodes. Notably, this phase contains a switch statement, which tests the first given value for equality, and a case is called depending on the value contained. If the number inputted does not match any of the values, then the bomb explodes.  

**Phase 4** gets a bit more complicated, and involves a recursive call on an input value. In this phase, `func4()` is called with 3 arguments, with the first number being read by sscanf, the second being 0, and the third 14. It contains a recursive function that initially does some arithmetic on its input value. It will then selectively perform one of two recursive calls until the base case is reached. The values of the two recursively called functions are added together and then returned. In short, the numbers should sum to 10, and the scanf value should be less than or equal to 14. If the first value is less than 14 (variable avg in func4), the recursive call is replaced by avg = 1.  

The recursive algorithm can be described as the following. Initially, `func4()` is called with the input value, a low value, and a high value.  The subsequent calls are dependent on whether or not the average is greater, less than, or equal to the given number. If the given number equals the average, the function returns. This is essentially a binary search function that sums up the elements it visits.  

There are some important lines to observe in the objdump. The line `lea    0x19ca(%rip),%rsi   # 0x555555556ded` loads the address for sscanf. We also observe 0x555555556ded  which reveals "%d %d", the two integers. Next we observe `cmp    $0x2,%eax` which assures that sscanf reads two values, and if it does not a call on bomb explode is made. Two additional checks are made, the first of which is `cmpl   $0xe,(%rsp)),%rdi`  which checks that the first value read by sscanf is <= 14, where 0xe = 14 in decimal, and the second `cmp   $0xa,%eax` checks that the second value read by sscanf does not equal 10, where    0xa = 10 in decimal.  

**Phase 5** involves taking two decimal integers as input through scanf. If the wrong number of integers are entered, the bomb explodes. If the first value doesn’t equal 15, it is placed into the array {10, 2, 14, 7, 8, 12, 15, 11, 0, 4, 1, 13, 3, 9, 6} that can be revealed by `print *0x555555556b60@15`. The values in this array are added to this value as long as the sum does not equal 15. Once the sum is 15, the appropriate second value is known, and the explosion is avoided.  

**Phase 6** reads in six numbers separated by spaces using the function `read_six_numbers`. The inputted numbers are checked to ensure that the numbers are unique from one another. There is a loop to ensure that each number is less than 6.  In observing the objdump, we can take a look at $rbx using the command `x/32 $rbx-32` to reveal 6 nodes. The nodes each contain 3 values, one of which is a data value storing a unique number. The nodes must be put in descending order of their data value, and each given index must be subtracted from 7 to get the correct order. For our bomb, the nodes found in descending order of value size were `1 4 2 6 5 3`. With each index subtracted from 7 giving us the order required for input: `6 3 5 1 2 4`.  

**Phase 7 (secret phase)** is accessed by appending DrEvil to the input for phase 4. The `phase_defused` function then calls the `secret_phase` function. The secret phase answer is 36, as indicated by the binary tree represented in memory which requires a mode of value 36.

## phases.c
Below is my recreation of the C code that is equivalent to the machine code in Phases 1 through 6.
<br />

**Phase 1**

```
void phase_1(char *input) {
        char *key = "“I am not part of the problem. I am a Republican";
        if (strings_not_equal(key, input) == 1) explode_bomb();
}
```
<br />

**Phase 2**

```
void phase_2(char *input) {
  int six_num[6];
  read_six_numbers(input, six_num);
  if(six_num[0] = 1)
  {
  	  for (int i = 2; i < 6; i++) {
        if (six_num[i] != 2*i-1) {
          explode_bomb();
        }
  		  else {
          return;
        }
      }               
  }
} 
```
<br />

**Phase 3**

```
void phase_3(char *input)
{
  int a,b; 

  //make sure 2 ints given
  if (sscanf(input, "%d %d", &a, &b) <= 1) explode_bomb(); 
  if (a > 8) explode_bomb(); //check validity of first int

  // there is some arithmetic to calculate the keys.
  // here is the general premise..
  // comparison values (cases)

  // while I am unsure what would start the specific case... %eax + $0x44 is the code that the statement executes where %eax is variable 'a' in this code

  Switch (a)
  {
    // I am not certain what is to be included in these cases ().
    case(a + 0x33c) {a - 0x33c};
    case(a - 0x44) {a + 0x44};
    case(a + 0x3d3) {a - 0x3d3};
    case(a - 0xd3) {a + 0xd3};
    case(a + 0xd3) {a - 0xd3};
    case(a - 0x3d3) {a + 0x3d3};
    case(a + 0x3d3) {a - 0x3d3};

    if (b != key) {
      explode_bomb();
    }
  }
}
```
<br />

**Phase 4**
```
int func4(int val, int low, int high) {
  int avg = low + (high - low) / 2;
	if (val < avg)
		avg += func4(val, low, avg - 1);
	else if (val > avg)
		avg += func4(val, avg + 1, high);
	else if (val == avg)
		return avg;
}

void phase_4(char *input) {
  int a,b; 
  if (sscanf(input, "%d %d", &a, &b)  != 2 || a >= 15)
    explode_bomb(); 
  if (func4(a, 0, 14) != 10 || b != 10)
    explode_bomb(); 
  return;
}
```
<br />

**Phase 5**

```
static int array[15] = { 10, 2, 14, 7, 8, 12, 15, 11, 0, 4, 1, 13, 3, 9, 6 };
void phase_5(char *input) {
    int a, b;
    if (sscanf(input, "%d %d", &a, &b) <= 1)
        explode_bomb();
    a &= 15;
    if (a != 15) {
        int c = 0, d = 0;
        do {
            d = d+1;
            a = array[a];
            c += a;
        } while (a != 15);
        if (d == 15 && c == b) return;
    }
    explode_bomb();
}
```
<br />

**Phase 6**

```
void phase_6(char *input){
  int six_num[6];
  read_six_numbers(input, six_num);
  if (sizeof(six_num) >= 7)
  	explode_bomb();
  // values must be between 1 and 6
  for (int i = 0; i < 6; i++)
  {
  	if (six_num[i] < 1 || six_num[i] > 6)
  		explode_bomb();
  }
  // guarantee unique input
  for (int i = 0; i < 6; i++)
  {
    for (int j = 0; j < 6; j++)
    {	
    		if (six_num[i] == six_num[j] && i != j)
    			explode_bomb();
    }
  }

  /*
  Node struct in global scope with nodes already created by binary bomb
  struct node {
      int value;
      int index;
      struct node *next
  };
  */

  for (int i = 0; i < 5; i++) {
  	int index = 7 - six_num[i];
  	int next_index = 7 - six_num[i+1];
  	// access node struct
  	Node curr_node = head;
  	// get current node
    while (curr_node->node != NULL) {
      if (curr_node->index == index) {
      	// find next node
      	Node next_node = head;
      	while (next_node->node != NULL) {
          if (next_node->index == next_index) {
             // make sure smaller than current 
             if (curr_node->next->value > curr_node->value)
              explode_bomb();
          }
          else
    	       break;
          next_node = next_node->next;
        }
    	}
    }
  	curr_node = curr_node->next;
  }
```
<br />
<br />

**Defuse File:**

I am not part of the problem. I am a Republican.

1 2 4 8 16 32

1 -1739

3 10

5 115

6 3 5 1 2 4